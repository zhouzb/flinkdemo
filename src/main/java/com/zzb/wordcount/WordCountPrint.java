package com.zzb.wordcount;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer011;

/**
 * @author zhouzongbo on 2019/3/21 22:31
 */
public class WordCountPrint {

    private static final String BROKER_LIST = "116.62.172.118:9092";
    public static void main(String[] args) throws Exception {
        // 检查输入参数
        final ParameterTool params = ParameterTool.fromArgs(args);

        // 设置执行环境
        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        //设置参数可用
        env.getConfig().setGlobalJobParameters(params);

        // 获取输入的数据
        DataStream<String> text;
        if (params.has("input")) {
            // 获取输入的内容
            text = env.readTextFile(params.get("input"));
        } else {
            System.out.println("Executing WordCount example with default input data set");
            System.out.println("Use --input to specify file input.");
            // 获取默认的文本数据 fromElements仅测试使用
            text = env.fromElements(WordCountData.WORD_COUNT_DATA);
        }

        // text.print();

        // 输出到kafka

        // 组装数据
        SingleOutputStreamOperator<String> operator = text.map(new MapFunction<String, String>() {
            @Override
            public String map(String value) throws Exception {
                return value;
            }
        });

        // 发送数据到kafka
         operator.addSink(new FlinkKafkaProducer011<String>(BROKER_LIST, "wiki", new SimpleStringSchema()));

        // 执行程序
        env.execute();
    }
}
