package com.zzb.wordcount;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.util.Collector;

/**
 * 分词器
 *
 * @author zhouzongbo on 2019/3/24 10:43
 */
public class Tokenizer implements FlatMapFunction<String, Tuple2<String, Integer>> {

    @Override
    public void flatMap(String value, Collector<Tuple2<String, Integer>> out) throws Exception {
        // 先把数字进行拆分
        String[] tokens = value.toLowerCase().split(",");

        // 通过2元组发送数据对
        for (String token : tokens) {
            if (!"".equals(token) && token != null && token.length() > 0) {
                out.collect(new Tuple2<>(token, 1));
            }
        }
    }
}
