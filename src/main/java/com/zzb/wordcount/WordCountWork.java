package com.zzb.wordcount;

import org.apache.flink.api.common.operators.Order;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.operators.FlatMapOperator;
import org.apache.flink.api.java.operators.SortedGrouping;
import org.apache.flink.api.java.operators.UnsortedGrouping;
import org.apache.flink.api.java.tuple.Tuple2;

import java.util.Arrays;

/**
 * WorkCount 大数据中的"Hello World", 首先把单词进行拆分，然后进行求和计算
 * @author zhouzongbo on 2019/3/24 10:37
 */
public class WordCountWork {

    public static void main(String[] args) throws Exception {
        // 创建流处理上下文
        // StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();

        // 由于本地测试， 所以数据直接写死.
        // 在服务器上测试该功能， 该字计数示例实现上述算法的输入参数：--input <path> --output <path>。
        // 作为测试数据，任何文本文件都可以
        // 此处本地测试， 修改官网的demo从本地读取数据
        DataSet<String> text = env.fromCollection(Arrays.asList(WordCountData.WORD_COUNT_DATA));
//        DataSet<Tuple2<String, Integer>> counts = text.flatMap(new Tokenizer())
//            // 分组: 根据2元组的第一个字段分组
//            .groupBy(0)
//            // 求和： 根据2元组的第二个字段求和
//            .sum(1);

        FlatMapOperator<String, Tuple2<String, Integer>> stringTuple2FlatMapOperator = text.flatMap(new Tokenizer());
        UnsortedGrouping<Tuple2<String, Integer>> tuple2UnsortedGrouping = stringTuple2FlatMapOperator.groupBy(0);
        SortedGrouping<Tuple2<String, Integer>> tuple2SortedGrouping = tuple2UnsortedGrouping.sortGroup(1, Order.DESCENDING);
        text.print();

    }
}
