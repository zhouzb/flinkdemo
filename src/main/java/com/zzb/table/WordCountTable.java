package com.zzb.table;

import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;


/**
 * Simple example for demonstrating the use of the Table API for a Word Count in Java.
 * *
 * * <p>This example shows how to:
 * *  - Convert DataSets to Tables
 * *  - Apply group, aggregate, select, and filter operations
 *
 * @author zhouzongbo on 2019/3/25 20:27
 */
public class WordCountTable {

    public static void main(String[] args) throws Exception {
        // 创建运行环境上下文
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        // 获取批量表运行环境
//        BatchTableEnvi
        //ronment tEnv = BatchTableEnvironment.getTableEnvironment(env);
//      、  // 获取输入元素
//        DataSet<WC> elements = env.fromElements(new WC("Hello", 1L),
//            new WC("deibi", 1L),
//            new WC("Bob", 1L));
//
//        // 返回一个Table 数据
//        Table table = tEnv.fromDataSet(elements);
//
//        Table filtered = table
//            // 根据word字段进行分组
//            .groupBy("word")
//            // 选择器
//            .select("word, frequency.sum as frequency")
//            // 过滤
//            .filter("frequency = 2");
//
//        // 组装返回数据
//        DataSet<WC> result = tEnv.toDataSet(filtered, WC.class);
//
//        result.print();

//        List<WC> wcs = Arrays.asList(new WC[]{new WC("HELLO", 1L),
//            new WC("W", 1L),
//            new WC("HELLO", 1l)});
//        DataStream<WC> wcDataSource = env.fromCollection(wcs);
//        wcDataSource.keyBy("word").sum("frequency");

//        wcDataSource.print();

    }

    private static class WC {
        /**
         * 单词
         */
        private String word;
        /**
         * 单词出现pinl
         */
        private long frequency;

        public WC(String word, long frequency) {
            this.word = word;
            this.frequency = frequency;
        }

        public WC() {

        }

        @Override
        public String toString() {
            return "WC{" +
                "word='" + word + '\'' +
                ", frequency=" + frequency +
                '}';
        }
    }
}
