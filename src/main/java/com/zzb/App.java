package com.zzb;

import org.apache.flink.api.common.functions.FoldFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.api.java.functions.KeySelector;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer011;
import org.apache.flink.streaming.connectors.wikiedits.WikipediaEditEvent;
import org.apache.flink.streaming.connectors.wikiedits.WikipediaEditsSource;


/**
 * demo运行
 * 1.打包命令: mvn clean package
 * 2.idea中运行命令: mvn exec:java -Dexec.mainClass=com.zzb.App
 * Hello world!
 */
public class App {
    public static void main(String[] args) throws Exception {
        // 创建执行程序源
        StreamExecutionEnvironment see = StreamExecutionEnvironment.getExecutionEnvironment();
        // 读取IRC日志中读取的源
        DataStreamSource<WikipediaEditEvent> edits = see.addSource(new WikipediaEditsSource());

        // 根据用户名进行KeyBy
        KeyedStream<WikipediaEditEvent, String> keyedEdits =
            edits.keyBy(new KeySelector<WikipediaEditEvent, String>() {
                @Override
                public String getKey(WikipediaEditEvent event) throws Exception {
                    return event.getUser();
                }
            });

        // 5秒钟进行一次聚合的窗口
        DataStream<Tuple2<String, Long>> result = keyedEdits.timeWindow(Time.seconds(5)).fold(new Tuple2<>("", 0L), new FoldFunction<WikipediaEditEvent, Tuple2<String, Long>>() {
                @Override
                public Tuple2<String, Long> fold(Tuple2<String, Long> acc, WikipediaEditEvent event) throws Exception {
                    acc.f0 = event.getUser();
                    // 元组的第二个参数计算长度
                    acc.f1 += event.getByteDiff();
                    return null;
                }
            }
        );

        // 打印输入到控制台
        // result.print();
        // 把数据发送到kafka上
//        result.map(new MapFunction<Tuple2<String,Long>, Object>() {
//            @Override
//            public Object map(Tuple2<String, Long> value) throws Exception {
//                return value.toString();
//            }
//        }).addSink(new FlinkKafkaProducer011<String>("116.62.172.118:9092",
//                    "wiki", new SimpleStringSchema()));

        result.map(new MapFunction<Tuple2<String,Long>, String>() {
                @Override
                public String map(Tuple2<String, Long> tuple) {
                    return tuple.toString();
                }
            })
            .addSink(new FlinkKafkaProducer011<String>("localhost:9092", "wiki", new SimpleStringSchema()));
//        System.out.println("发送数据到kafka");
        // 执行批处理
        see.execute();
    }
}
