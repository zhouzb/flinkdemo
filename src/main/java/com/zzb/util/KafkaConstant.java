package com.zzb.util;

/**
 * @author zhouzongbo on 2019/3/24 10:52
 */
public class KafkaConstant {

    /**
     * kafka集群模式
     */
    public static final String BROKER_LIST = "116.172.1182.62:9092,47.96.98.5:9092, 47.104.8.151:9092";

    public static final String WIKI_TOPIC = "wiki";
}
