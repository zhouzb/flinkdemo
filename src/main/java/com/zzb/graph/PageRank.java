package com.zzb.graph;

import com.zzb.graph.util.PageRankData;
import org.apache.flink.api.common.functions.FilterFunction;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.aggregation.Aggregations;
import org.apache.flink.api.java.operators.IterativeDataSet;
import org.apache.flink.api.java.tuple.Tuple1;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.util.Collector;

import java.util.ArrayList;
import java.util.List;

/**
 * 网页排序]
 * 所述的PageRank程序实现上述实施例。它需要运行以下参数：
 * --pages <path> --links <path> --output <path> --numPages <n> --iterations <n>。
 *
 * PageRank算法计算链接定义的图形中页面的“重要性”，链接指向一个页面到另一个页面。它是一种迭代图算法，
 * 这意味着它重复应用相同的计算。在每次迭代中，每个页面在其所有邻居上分配其当前等级，
 * 并将其新等级计算为从其邻居接收的等级的总和。PageRank算法由Google搜索引擎推广，
 * 该搜索引擎利用网页的重要性对搜索查询的结果进行排名。
 *
 * 在这个简单的例子中，PageRank通过批量迭代和固定数量的迭代来实现。
 * @author zhouzongbo on 2019/3/24 11:53
 */
public class PageRank {

    /**
     * 抑制因子
     */
    private static final double DAMPENING_FACTOR = 0.85;

    /**
     * 希腊语字母之第五字
     */
    private static final double EPSILON = 0.0001;

    public static void main(String[] args) throws Exception {
        // 获取参数: --pages <path> --links <path> --output <path> --numPages <n> --iterations <n>， 通过"-- or -"解析
        ParameterTool params = ParameterTool.fromArgs(args);

        // 排名数:  首先获取输入参数中的值， 如果不存在， 则使用默认的
        final int numPages = params.getInt("numPages", PageRankData.getNumberOfPages());
        // 迭代次数
        final int maxIterations = params.getInt("iterations", 10);

        // 创建一个执行环境
        final ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();

        // 使这些参数对web ui可用; 注册一个自定义的、可序列化的用户配置对象
        env.getConfig().setGlobalJobParameters(params);

        // env.getConfig().setExecutionMode(ExecutionMode.BATCH);
        // 获取输入的数据
        DataSet<Long> pagesInput = getPagesDataSet(env, params);
        DataSet<Tuple2<Long, Long>> linkInput = getLinksDataSet(env, params);

        // 为页面分配初始化排名
        DataSet<Tuple2<Long, Double>> pagesWithRanks =
            pagesInput.map(new RankAssigner(1.0d / numPages));

        // 从链接输入构建邻接表
        DataSet<Tuple2<Long, Long[]>> adjacencyListInput = linkInput.groupBy(0).reduceGroup(new BuildOutgoingEdgeList());

        // 迭代数据集
        IterativeDataSet<Tuple2<Long, Double>> iteration = pagesWithRanks.iterate(maxIterations);

        // 获取新的排名
        DataSet<Tuple2<Long, Double>> newRanks = iteration
            // join pages with outgoing edges and distribute rank
            .join(adjacencyListInput).where(0).equalTo(0).flatMap(new JoinVertexWithEdgesMatch())
            // collect and sum ranks
            .groupBy(0).aggregate(Aggregations.SUM , 1)
            .map(new Dampener(DAMPENING_FACTOR, numPages));

        DataSet<Tuple2<Long, Double>> finalPageRanks = iteration.closeWith(
            newRanks,
            newRanks.join(iteration).where(0).equalTo(0)
                // termination condition
                .filter(new EpsilonFilter())
        );


        if (params.has("output")) {
            finalPageRanks.writeAsCsv(params.get("output"), "\n", "");
            env.execute();
        } else {
            finalPageRanks.print();
        }


    }

    /**
     * 获取分页的数据集合
     *
     * @param env    env
     * @param params params
     * @return DataSet
     */
    private static DataSet<Long> getPagesDataSet(ExecutionEnvironment env, ParameterTool params) {

        // 从运行环境中获取page
        if (params.has("pages")) {
            return env.readCsvFile(params.get("pages"))
                // csv参数设置: 分隔符
                .fieldDelimiter(" ")
                // csv参数设置: 每行的分隔符
                .lineDelimiter("\n")
                // 制定cvs的参数类型， 并将csv转换为一个一元组
                .types(Long.class)
                // 返回数据
                // 泛型类型: (MapFunction<Tuple1<Long>, Long>)
                // 方法体中的参数: (value)
                .map((MapFunction<Tuple1<Long>, Long>) (value) -> value.f0
                );
        } else {
            System.out.println("Executing PageRank example with default data set");
            System.out.println("User --page to specify file input.");
            // 获取默认的排序数据: 创建一个新的
            return PageRankData.getDefaultPagesDataSet(env);
        }
    }


    /**
     * 通过map function 为所有页面分配初始级
     */
    private static final class RankAssigner implements MapFunction<Long, Tuple2<Long, Double>> {

        // 输出网页和排名
        private Tuple2<Long, Double> outPageWithRank;

        public RankAssigner(double rank) {
            this.outPageWithRank = new Tuple2<>(-1L, rank);
        }

        @Override
        public Tuple2<Long, Double> map(Long page) throws Exception {
            outPageWithRank.f0 = page;
            return outPageWithRank;
        }
    }

    /**
     * 获取链接表
     *
     * @param env
     * @param params
     * @return
     */
    private static DataSet<Tuple2<Long, Long>> getLinksDataSet(ExecutionEnvironment env, ParameterTool params) {

        if (params.has("links")) {
            return env
                .readCsvFile(params.get("links"))
                .fieldDelimiter(" ")
                .lineDelimiter("\n")
                .types(Long.class, Long.class);
        } else {
            System.out.println("Executing PageRank example with default link data set");
            return PageRankData.getDefaultEdgeDataSet(env);
        }
    }


    /**
     * 一个reduce函数，它接受一系列边，并为边所在的顶点构建邻接表
     * 产生。作为预处理步骤运行
     */
    private static class BuildOutgoingEdgeList implements GroupReduceFunction<Tuple2<Long, Long>, Tuple2<Long, Long[]>> {

        /**
         * 相邻元素集合
         */
        private final List<Long> neighbors = new ArrayList<>();

        @Override
        public void reduce(Iterable<Tuple2<Long, Long>> values, Collector<Tuple2<Long, Long[]>> out) throws Exception {
            // 清空集合
            neighbors.clear();
            // 网页的ID
            Long id = 0L;

            for (Tuple2<Long, Long> value : values) {
                id = value.f0;
                neighbors.add(value.f1);
            }
            out.collect(new Tuple2<>(id, neighbors.toArray(new Long[neighbors.size()])));
        }
    }

    /**
     * 连接函数，将顶点秩的一部分分配给所有邻居
     * 输入一个(BuildOutgoingEdgeList)Tuple2<Tuple2<Long, Double>,
     * Tuple2<Long, Long[]>>, 返回Tuple2<Long, Double>
     *
     */
    private static final class JoinVertexWithEdgesMatch implements
        FlatMapFunction<Tuple2<Tuple2<Long, Double>, Tuple2<Long, Long[]>>, Tuple2<Long, Double>> {

        @Override
        public void flatMap(Tuple2<Tuple2<Long, Double>, Tuple2<Long, Long[]>> value,
                            Collector<Tuple2<Long, Double>> out) throws Exception {
            // 第一个二元组中的第二个元组中的第二个元组集合
            Long[] neighbors =value.f1.f1;

            // 获取排名
            double rank = value.f0.f1;

            // 排名分配
            double rankToDistribute = rank / ((double) neighbors.length);

            // 迭代
            for (Long neighbor : neighbors) {
                out.collect(new Tuple2<>(neighbor, rankToDistribute));
            }
        }
    }

    private static final class Dampener implements MapFunction<Tuple2<Long, Double>, Tuple2<Long, Double>> {

        private final double dampening;
        private final double randomJump;

        public Dampener(double dampening, double numVertices) {
            this.dampening = dampening;
            this.randomJump = (1- dampening) / numVertices;
        }

        @Override
        public Tuple2<Long, Double> map(Tuple2<Long, Double> value) throws Exception {
            value.f1 = (value.f1 * dampening) + randomJump;
            return value;
        }
    }


    /**
     * 筛选器，筛选秩差低于阈值的顶点
     */
    private static final class EpsilonFilter implements FilterFunction<Tuple2<Tuple2<Long, Double>, Tuple2<Long, Double>>> {

        @Override
        public boolean filter(Tuple2<Tuple2<Long, Double>, Tuple2<Long, Double>> value) throws Exception {
            return Math.abs(value.f0.f1 - value.f1.f1) > EPSILON;
        }
    }
}
