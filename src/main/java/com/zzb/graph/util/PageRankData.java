package com.zzb.graph.util;

import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.tuple.Tuple2;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zhouzongbo on 2019/3/24 11:57
 */
public class PageRankData {

    public static final Object[][] EDGES = {
        {1L, 2L},
        {1L, 15L},
        {2L, 3L},
        {2L, 4L},
        {2L, 5L},
        {2L, 6L},
        {2L, 7L},
        {3L, 13L},
        {4L, 2L},
        {5L, 11L},
        {5L, 12L},
        {6L, 1L},
        {6L, 7L},
        {6L, 8L},
        {7L, 1L},
        {7L, 8L},
        {8L, 1L},
        {8L, 9L},
        {8L, 10L},
        {9L, 14L},
        {9L, 1L},
        {10L, 1L},
        {10L, 13L},
        {11L, 12L},
        {11L, 1L},
        {12L, 1L},
        {13L, 14L},
        {14L, 12L},
        {15L, 1L},
    };

    /**
     * 分页数量
     */
    private static final Integer numPages = 15;

    /**
     * 获取默认的DataSet数据
     * @param env
     * @return
     */
    public static DataSet<Tuple2<Long, Long>> getDefaultEdgeDataSet(ExecutionEnvironment env) {
        List<Tuple2<Long, Long>> edge = new ArrayList<>();

        for (Object[] e: EDGES) {
            edge.add(new Tuple2<>((Long) e[0], (Long) e[1]));
        }

        // 存入排名数据到运行环境中
        return env.fromCollection(edge);
    }

    /**'
     * 从运行环境中取出之前存入的数据, 由于此处可以在并行环境中创建， 因此不能保证元素的顺序
     * @param env 运行环境上下文
     * @return DataSet
     */
    public static DataSet<Long> getDefaultPagesDataSet(ExecutionEnvironment env) {
        //  从运行环境中取出之前存入的数据, 由于此处可以在并行环境中创建， 因此不能保证元素的顺序
        return env.generateSequence(1, numPages);
    }

    public static int getNumberOfPages() {
        return numPages;
    }
}
