package com.zzb.stream;

import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

/**
 * 这个例子实现了一个穷人的计数窗口。我们通过第一个字段键入元组（在示例中都具有相同的键1）。
 * 该函数将计数和运行总和存储在a中ValueState。一旦计数达到2，它将发出平均值并清除状态，
 * 以便我们重新开始0。请注意，如果我们在第一个字段中具有不同值的元组，则会为每个不同的输入键保留不同的状态值。
 * @author zhouzongbo on 2019/3/25 22:36
 */
public class TestStream {
    public static void main(String[] args) throws Exception {
        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        env.fromElements(Tuple2.of(1L, 3L), Tuple2.of(1L, 5L), Tuple2.of(1L, 7L), Tuple2.of(1L, 4L), Tuple2.of(1L, 2L))
            .keyBy(0)
            .flatMap(new CountWindowAverage())
            .print();
        // 要想在flink集群环境中运行, 需要增加execute()
        // env.execute();
    }

//    /**
//     * 有状态的map
//     */
//    public static class CountWindowAverage extends RichFlatMapFunction<Tuple2<Long, Long>, Tuple2<Long, Long>> {
//
//        /**
//         * ValueState; 第一个字段用于求和(count)，第二个字段用于累加(sum)
//         */
//        private transient ValueState<Tuple2<Long, Long>> sum;
//        @Override
//        public void flatMap(Tuple2<Long, Long> input, Collector<Tuple2<Long, Long>> out) throws Exception {
//
//            // 访问状态值
//            Tuple2<Long, Long> currentSum  = sum.value();
//
//            // 更新count
//            currentSum.f0 += 1;
//
//            //add the second field of the input value
//            currentSum.f1 += input.f1;
//
//            // 更新ValueState
//            sum.update(currentSum);
//
//            // if the count reaches 2, emit the average and clear the state
//            if (currentSum.f0 >= 2) {
//                // 发射参数: input.f0: 输入参数的序号, currentSum: 当前参数进行求平均值
//                out.collect(new Tuple2<>(input.f0, currentSum.f1 / currentSum.f0));
//                sum.clear();
//            }
//        }
//
//        /**
//         * 第一次运行程序的时候初始化
//         * @param config config
//         * @throws Exception Exception
//         */
//        @Override
//        public void open(Configuration config) throws Exception {
//
//            ValueStateDescriptor<Tuple2<Long, Long>> valueStateDescriptor = new ValueStateDescriptor<>(
//                "my-default", TypeInformation.of(new TypeHint<Tuple2<Long, Long>>() {})
//            );
//            // 从运行环境中获取状态
//            sum = getRuntimeContext().getState(valueStateDescriptor);
//        }
//    }
}
